import { HomePage } from './../pages/home/home';
import { ListaAmbientesPage } from './../pages/lista-ambientes/lista-ambientes';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListaPokemonsPage } from '../pages/lista-pokemons/lista-pokemons';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  abrirPagina(pagina: string): void {
    switch(pagina){
      case 'ambientes':
        this.nav.setRoot(ListaAmbientesPage);
        break;
      case 'pokemons':
        this.nav.setRoot(ListaPokemonsPage);
        break;
      case 'zoo':
        this.nav.setRoot(HomePage);
        break;

      default:
        this.nav.setRoot(ListaAmbientesPage);
    }
  }
}

