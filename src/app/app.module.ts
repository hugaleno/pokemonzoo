import { LinhaComponent } from './../components/linha/linha';
import { HomePage } from './../pages/home/home';
import { FiltraPokemonTipoPipe } from './../pipes/filtra-pokemon-tipo/filtra-pokemon-tipo';
import { ConfiguracoesComponent } from './../components/configuracoes/configuracoes-component';
import { PokemonProvider } from './../providers/pokemon-provider/pokemon.provider';
import { AdicionarPokemonModalPage } from './../pages/adicionar-pokemon-modal/adicionar-pokemon-modal';
import { PokemonListaSimplesComponent } from './../components/pokemon-lista-simples/pokemon-lista-simples';
import { AmbientePage } from './../pages/ambiente/ambiente.page';
import { AmbienteProvider } from './../providers/ambiente-provider/ambiente.provider';
import { ListaAmbientesComponent } from './../components/lista-ambientes/lista-ambientes.component';
import { AdicionarAmbienteModalPage } from './../pages/adicionar-ambiente-modal/adicionar-ambiente-modal';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ListaAmbientesPage } from '../pages/lista-ambientes/lista-ambientes';
import { CustomHeaderMenuComponent } from '../components/custom-header-menu/custom-header-menu';
import { IonicStorageModule } from '@ionic/storage';
import { ListaPokemonsPage } from '../pages/lista-pokemons/lista-pokemons';

@NgModule({
  declarations: [
    MyApp,
    ListaAmbientesPage,
    CustomHeaderMenuComponent,
    AdicionarAmbienteModalPage,
    ListaAmbientesComponent,
    AmbientePage,
    PokemonListaSimplesComponent,
    AdicionarPokemonModalPage,
    ConfiguracoesComponent,
    ListaPokemonsPage,
    FiltraPokemonTipoPipe,
    HomePage,
    LinhaComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name:'__zoo'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListaAmbientesPage,
    AdicionarAmbienteModalPage,
    AmbientePage,
    AdicionarPokemonModalPage,
    ListaPokemonsPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AmbienteProvider,
    PokemonProvider
  ]
})
export class AppModule {}
