export const LOG_ON:boolean = false;

export const AGUA:string = "agua";
export const FOGO:string = "fogo";
export const GRAMA:string = "grama";
export const ELETRICO:string = "eletrico";

export const FOTOS_TIPO_FOGO: string[] = [
    'assets/imgs/tipo_fire/charmander.jpg',
    'assets/imgs/tipo_fire/cyndaquil.jpg',
    'assets/imgs/tipo_fire/flareon.jpg',
    'assets/imgs/tipo_fire/growlithe.jpg',
    'assets/imgs/tipo_fire/houndour.jpg',
    'assets/imgs/tipo_fire/magmar.jpg',
    'assets/imgs/tipo_fire/moltres.jpg',
    'assets/imgs/tipo_fire/ninetales.jpg',
    'assets/imgs/tipo_fire/ponyta.jpg',
    'assets/imgs/tipo_fire/vulpix.jpg'
];


export const FOTOS_TIPO_AGUA: string[] = [
    'assets/imgs/tipo_water/cloyster.jpg',
    'assets/imgs/tipo_water/golduck.jpg',
    'assets/imgs/tipo_water/gyarados.jpg',
    'assets/imgs/tipo_water/krabby.jpg',
    'assets/imgs/tipo_water/poliwag.jpg',
    'assets/imgs/tipo_water/seadra.jpg',
    'assets/imgs/tipo_water/seel.jpg',
    'assets/imgs/tipo_water/squirtle.jpg',
    'assets/imgs/tipo_water/staryu.jpg',
    'assets/imgs/tipo_water/totodile.jpg'
];


export const FOTOS_TIPO_ELETRICO: string[] = [
    'assets/imgs/tipo_electric/ampharos.jpg',
    'assets/imgs/tipo_electric/electabuzz.jpg',
    'assets/imgs/tipo_electric/electrode.jpg',
    'assets/imgs/tipo_electric/magneton.jpg',
    'assets/imgs/tipo_electric/manectric.jpg',
    'assets/imgs/tipo_electric/mareep.jpg',
    'assets/imgs/tipo_electric/pichu.jpg',
    'assets/imgs/tipo_electric/pikachu.jpg',
    'assets/imgs/tipo_electric/raikou.jpg',
    'assets/imgs/tipo_electric/zapdos.jpg'
];


export const FOTOS_TIPO_GRAMA: string[] = [
    'assets/imgs/tipo_grass/bellossom.jpg',
    'assets/imgs/tipo_grass/bulbasaur.jpg',
    'assets/imgs/tipo_grass/cacnea.jpg',
    'assets/imgs/tipo_grass/chikorita.jpg',
    'assets/imgs/tipo_grass/oddish.jpg',
    'assets/imgs/tipo_grass/seedot.jpg',
    'assets/imgs/tipo_grass/sunflora.jpg',
    'assets/imgs/tipo_grass/sunkern.jpg',
    'assets/imgs/tipo_grass/tangela.jpg',
    'assets/imgs/tipo_grass/treecko.jpg'
];