import { FOTOS_TIPO_AGUA, FOTOS_TIPO_GRAMA, GRAMA, FOTOS_TIPO_ELETRICO } from './../app/constantes';
import { AGUA, FOGO, FOTOS_TIPO_FOGO, ELETRICO } from './../app/constantes';
export class Pokemon {
    public id : number;
    public nome: string;
    public foto: string;

    constructor(
        public ambienteId: number,
        public tipo: string
    ){
        this.id = new Date().getTime();
        this.selecionaFoto();
    }

    selecionaFoto(): void {
        let pos = this.geraPosicao();
        switch(this.tipo){
            case AGUA:
                this.foto = FOTOS_TIPO_AGUA[pos];
            break;
            case FOGO:
                this.foto = FOTOS_TIPO_FOGO[pos];
            break;
            case GRAMA:
                this.foto = FOTOS_TIPO_GRAMA[pos];
            break;
            case ELETRICO:
                this.foto = FOTOS_TIPO_ELETRICO[pos];
            break;
            default:
                this.foto="assets/imgs/logo.png"
        }
    }
    geraPosicao(): number {
        return Math.floor(Math.random()*10);
    }
}