import { AGUA } from "../app/constantes";
export class Ambiente {
    public id: number;
    public numPokemons: number;
    public ativado: boolean;
    public nome: string;
    public temperatura: number;
    public tipo: string;

    constructor() {
        this.id = new Date().getTime();
        this.numPokemons = 0;
        this.ativado = true;
        this.temperatura = 10;
        this.tipo = AGUA;
    }
}