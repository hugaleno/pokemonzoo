import { NavController } from 'ionic-angular';
import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { Ambiente } from './../../models/ambiente';
import { Component, Input } from '@angular/core';
import { AmbientePage } from '../../pages/ambiente/ambiente.page';

@Component({
  selector: 'lista-ambientes-component',
  templateUrl: 'lista-ambientes.component.html'
})
export class ListaAmbientesComponent {
  
  @Input() ambientes: Ambiente[];
  @Input() tipo: string;

  constructor(
    public ambienteProvider: AmbienteProvider,
    public navCtrl: NavController
  ) {
    
  }

  goAmbiente(ambiente: Ambiente): void {
    this.navCtrl.push(AmbientePage,{
      ambienteClicado: ambiente
    });
  }

  deletar( ambiente: Ambiente ): void {
    this.ambienteProvider.excluir(ambiente)
      .then( () => {
        this.ambientes.splice(this.ambientes.indexOf(ambiente),1);
        //console.log(`Ambiente ${ambiente.nome} excluído`);
      });
  }

}
