import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { Ambiente } from './../../models/ambiente';
import { PokemonProvider } from './../../providers/pokemon-provider/pokemon.provider';
import { Pokemon } from './../../models/pokemon';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'pokemon-lista-simples',
  templateUrl: 'pokemon-lista-simples.html'
})
export class PokemonListaSimplesComponent {

  @Input() pokemons: Pokemon[];
  @Input() ambiente: Ambiente;
  
  constructor(
    public pokemonProvider:PokemonProvider,
    public ambienteProvider: AmbienteProvider
  ) {
  }

  excluir(pokemon: Pokemon): void {
    this.pokemonProvider.deletar(pokemon)
      .then( () => {
          this.ambiente.numPokemons--;
          this.ambienteProvider.save(this.ambiente);
          this.pokemons.splice(this.pokemons.indexOf(pokemon),1);
      });
  }

}
