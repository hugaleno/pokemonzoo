import { Component, Input } from '@angular/core';

@Component({
  selector: 'custom-header-menu',
  templateUrl: 'custom-header-menu.html'
})
export class CustomHeaderMenuComponent {

  @Input() titulo: string;

  constructor() {
  }

}
