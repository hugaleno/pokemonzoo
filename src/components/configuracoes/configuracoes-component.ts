import { ToastController, AlertController } from 'ionic-angular';
import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { Component, Input } from '@angular/core';
import { Ambiente } from '../../models/ambiente';

@Component({
  selector: 'configuracoes-component',
  templateUrl: 'configuracoes-component.html'
})
export class ConfiguracoesComponent {

  @Input() ambiente: Ambiente;
  @Input('corBotao') cor: string;

  constructor(
    public ambienteProvider: AmbienteProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
  }

  salvarAmbiente(): void {
    this.ambienteProvider.save(this.ambiente)
      .then( ()=> {
        this.showToast("Informações atualizadas");
      })
      .catch( ()=> {
        this.showAlert(`Erro ao salvar ambiente ${this.ambiente.nome}`);
      });
  }
  showAlert(mensagem: string):void {
    this.alertCtrl.create({
      title:"Erro",
      message: mensagem,
      buttons:['Ok']
    }).present();
  }

  showToast(mensagem: string): void {
    this.toastCtrl.create({
      message: mensagem,
      position:'center',
      duration:3000
    }).present();
  }
}
