import { Component, Input } from '@angular/core';

@Component({
  selector: 'linha',
  templateUrl: 'linha.html'
})
export class LinhaComponent {

  @Input() label;
  @Input() cor;
  @Input() quantidade;

  constructor() {

  }

}
