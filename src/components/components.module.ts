import { NgModule } from '@angular/core';
import { LinhaComponent } from './linha/linha';
@NgModule({
	declarations: [LinhaComponent],
	imports: [],
	exports: [LinhaComponent]
})
export class ComponentsModule {}
