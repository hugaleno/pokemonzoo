import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';

import { Ambiente } from './../../models/ambiente';
import { AdicionarAmbienteModalPage } from './../adicionar-ambiente-modal/adicionar-ambiente-modal';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, ToastController } from 'ionic-angular';
import { GRAMA, FOGO, ELETRICO, AGUA } from '../../app/constantes';


@Component({
  selector: 'page-lista-ambientes',
  templateUrl: 'lista-ambientes.html',
})
export class ListaAmbientesPage {
  
  ambientesFogo: Ambiente[];
  ambientesAgua: Ambiente[];
  ambientesGrama: Ambiente[];
  ambientesEletrico: Ambiente[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public ambienteProvider: AmbienteProvider
  ) 
  { 
    this.ambientesAgua=[];
    this.ambientesGrama=[];
    this.ambientesFogo=[];
    this.ambientesEletrico=[];
  }

  ionViewDidEnter(): void {
    this.ambienteProvider.getAll()
      .then( ( ambientes: Ambiente[] ) => {
        this.ambientesAgua = 
          ambientes.filter(
            (ambiente:Ambiente, 
              index: number, 
              ambientes:Ambiente[]) => ambiente.tipo == AGUA);

        this.ambientesFogo = 
          ambientes.filter(
            (ambiente:Ambiente, 
              index: number, 
              ambientes:Ambiente[]) => ambiente.tipo == FOGO);

        this.ambientesGrama = 
          ambientes.filter(
            (ambiente:Ambiente, 
              index: number, 
              ambientes:Ambiente[]) => ambiente.tipo == GRAMA);
        this.ambientesEletrico = 
          ambientes.filter(
            (ambiente:Ambiente, 
              index: number, 
              ambientes:Ambiente[]) => ambiente.tipo == ELETRICO);
      });
  }

  addAmbiente(): void {
    let addModal = this.modalCtrl.create(AdicionarAmbienteModalPage);
    addModal.onDidDismiss( ambiente => {
      if(ambiente){
        this.toastCtrl.create({
          message:`Ambiente ${ambiente.nome} adicionado.`,
          position:'bottom',
          duration: 3000
        }).present();
        switch(ambiente.tipo){
          case FOGO:
            this.ambientesFogo.push(ambiente);
            break;
          case AGUA:
            this.ambientesAgua.push(ambiente);
            break;
          case GRAMA:
            this.ambientesGrama.push(ambiente);
            break;
          case ELETRICO:
            this.ambientesEletrico.push(ambiente);
            break;

          default:
            console.log("Ambiente não cadastrado");
        }
       
      }else{
        console.log("Usuario desistiu");
      }
    });
    addModal.present();
  }

  
}
