import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { Ambiente } from './../../models/ambiente';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-adicionar-ambiente-modal',
  templateUrl: 'adicionar-ambiente-modal.html',
})
export class AdicionarAmbienteModalPage {

  ambiente: Ambiente;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public ambienteProvider: AmbienteProvider
  ) {
    this.ambiente = new Ambiente();
  }

  adicionarAmbiente(): void {
    if(this.validarAmbiente(this.ambiente)){
      this.ambienteProvider.save(this.ambiente)
        .then( ()=> {
          this.dismiss(true);
        })
        .catch( () => {
          console.log("Erro ao salvar ambiente.");
          this.dismiss(false);
        });
      
    }else{
      this.showAlert("Campo Nome inválido. "+
        "Deve conter pelo menos 4 caracteres.");
    }
  }
  
  showAlert(mensagem: string):void {
    this.alertCtrl.create({
      title:"Erro",
      message: mensagem,
      buttons:['Ok']
    }).present();
  }

  dismiss( sucesso: boolean ): void {
    if(sucesso){
      this.viewCtrl.dismiss(this.ambiente);
    }else{
      this.viewCtrl.dismiss();
    }
    
  }

  validarAmbiente(ambiente: Ambiente): boolean {
      return  ambiente.nome && ambiente.nome.length > 3;
  }
}
