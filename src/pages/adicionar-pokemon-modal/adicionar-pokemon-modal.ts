import { PokemonProvider } from './../../providers/pokemon-provider/pokemon.provider';
import { Pokemon } from './../../models/pokemon';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Ambiente } from '../../models/ambiente';

@Component({
  selector: 'page-adicionar-pokemon-modal',
  templateUrl: 'adicionar-pokemon-modal.html',
})
export class AdicionarPokemonModalPage {

  pokemon: Pokemon;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public pokemonProvider: PokemonProvider
  ) {
    let ambiente: Ambiente = navParams.get('ambiente');
    this.pokemon = new Pokemon(ambiente.id, ambiente.tipo);
  }

  dismiss(sucesso: boolean): void {
    if(sucesso){
      this.viewCtrl.dismiss(this.pokemon);
    }else{
      this.viewCtrl.dismiss();
    }
    
  }
  addPokemon(): void {
    if(this.validaPokemon()){
      this.pokemonProvider.save(this.pokemon)
        .then( (pokemon: Pokemon)=> {
          console.log("Pokemon "+ pokemon.nome + " salvo!");
          console.log(`Pokemon ${pokemon.nome} salvo!`);
          this.dismiss(true);
        });
      
    }else{
      this.showAlert("Campo Nome inválido. Mínimo 3 caracteres!");
    }
    
  }

  showAlert(mensagem: string): void {
    this.alertCtrl.create({
      title:'Erro',
      message: mensagem,
      buttons:['Ok']
    }).present();
  }

  validaPokemon(): boolean {
    return this.pokemon.nome && this.pokemon.nome.length >= 3 ;
  }

}
