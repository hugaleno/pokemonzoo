import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { PokemonProvider } from './../../providers/pokemon-provider/pokemon.provider';
import { AGUA, GRAMA, ELETRICO, FOGO } from './../../app/constantes';
import { Ambiente } from './../../models/ambiente';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { AdicionarPokemonModalPage } from '../adicionar-pokemon-modal/adicionar-pokemon-modal';
import { Pokemon } from '../../models/pokemon';



@Component({
  selector: 'page-ambiente',
  templateUrl: 'ambiente.page.html',
})
export class AmbientePage {

  aba: string = 'pokemons';
  ambiente: Ambiente;
  pokemons: Pokemon[];
  cor: string = "primary";
  corBotoes: string = "secondary";

  constructor( 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public pokemonProvider: PokemonProvider,
    public ambienteProvider: AmbienteProvider,
    public toastCtrl: ToastController
  ) {
    this.ambiente = navParams.get("ambienteClicado");
    this.pokemons = [];
    this.setCores(this.ambiente.tipo);
  }

  setCores(tipo: string): void {
    switch(tipo){
      case AGUA:
        this.cor = "ambienteagua";
        this.corBotoes = "agua";
        break;
      case FOGO:
        this.cor="ambientefogo";
        this.corBotoes="danger";
        break;
      case GRAMA:
        this.cor="ambientegrama";
        this.corBotoes="darkprimary";
        break;
      case ELETRICO:
        this.cor="ambienteeletrico";
        this.corBotoes="secondarytext";
        break;
      default:
        this.cor = "primary"; 
    }
  }

  ionViewDidEnter(): void {
    this.pokemonProvider.getAll()
      .then( (pokemons: Pokemon[]) => {
       /*  let pokemonsAmbiente: Pokemon[] = [];

        for (let i = 0; i < pokemons.length; i++) {
          if( this.ambiente.id == pokemons[i].ambienteId ){
            pokemonsAmbiente.push(pokemons[i]);
          } 
          
        }

        this.pokemons = pokemonsAmbiente; */
         this.pokemons = pokemons.filter(
          (pokemon: Pokemon, index: number, pokemons)=>  {

          return this.ambiente.id == pokemon.ambienteId;
        });
      });
  }

  abrirAddPokemonModal(): void {
    let modal = this.modalCtrl.create(AdicionarPokemonModalPage,
      { 
        "ambiente": this.ambiente
      }
    );
    modal.onDidDismiss( (pokemon: Pokemon) => {
      if(pokemon){
        //this.ambiente.numPokemons =  this.ambiente.numPokemons +1;
        this.ambiente.numPokemons++;
        this.ambienteProvider.save(this.ambiente);
        this.pokemons.push(pokemon);
        this.toastCtrl.create({
          message: `Pokemon ${pokemon.nome} adicionado.`,
          position: 'bottom',
          duration: 3000
        }).present();
      }
      
    });
    modal.present();
  }

}
