import { PokemonProvider } from './../../providers/pokemon-provider/pokemon.provider';
import { Component } from '@angular/core';
import { Pokemon } from '../../models/pokemon';

@Component({
  selector: 'page-lista-pokemons',
  templateUrl: 'lista-pokemons.html',
})
export class ListaPokemonsPage {

  pokemons: Pokemon[];

  constructor(
    public pokemonProvider: PokemonProvider
  ) {
    this.pokemons = [];
  }

  ionViewDidEnter(): void {
    this.pokemonProvider.getAll()
      .then( (pokemons: Pokemon[])=> {
        this.pokemons = pokemons;
      });
  }

}
