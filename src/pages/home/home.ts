import { PokemonProvider } from './../../providers/pokemon-provider/pokemon.provider';
import { FOGO, AGUA, GRAMA, ELETRICO } from './../../app/constantes';
import { AmbienteProvider } from './../../providers/ambiente-provider/ambiente.provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Ambiente } from '../../models/ambiente';
import { asTextData } from '@angular/core/src/view';
import { Pokemon } from '../../models/pokemon';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  infoAmbiente;
  infoPokemons;
  
  constructor(
    public ambienteProvider: AmbienteProvider,
    public pokemonProvider: PokemonProvider
  ) {
    this.infoAmbiente = {
      ambientesAtuais: 0,
      ambientesFogo: 0,
      ambientesAgua: 0,
      ambientesGrama: 0,
      ambientesEletrico: 0
    };
    this.infoPokemons = {
      pokemonsAtuais: 0,
      pokemonsFogo: 0,
      pokemonsAgua: 0,
      pokemonsGrama: 0,
      pokemonsEletrico: 0
    };
  }

  ionViewWillEnter(): void {
    this.ambienteProvider.getAll()
      .then((ambientes: Ambiente[]) => {
        this.infoAmbiente.ambientesAtuais = ambientes.length;

        this.infoAmbiente.ambientesFogo =
          ambientes.filter(
            (ambiente: Ambiente) =>
              ambiente.tipo == FOGO).length;

        this.infoAmbiente.ambientesAgua =
          ambientes.filter(
            (ambiente: Ambiente) =>
              ambiente.tipo == AGUA).length;

        this.infoAmbiente.ambientesGrama =
          ambientes.filter(
            (ambiente: Ambiente) =>
              ambiente.tipo == GRAMA).length;

        this.infoAmbiente.ambientesEletrico =
          ambientes.filter(
            (ambiente: Ambiente) =>
              ambiente.tipo == ELETRICO).length;
      });

    this.pokemonProvider.getAll()
      .then((pokemons: Pokemon[]) => {
        this.infoPokemons.pokemonsAtuais = pokemons.length;

        this.infoPokemons.pokemonsFogo =
          pokemons.filter(
            (pokemon: Pokemon) => pokemon.tipo == FOGO).length;

        this.infoPokemons.pokemonsAgua =
          pokemons.filter(
            (pokemon: Pokemon) => pokemon.tipo == AGUA).length;

        this.infoPokemons.pokemonsGrama =
          pokemons.filter(
            (pokemon: Pokemon) => pokemon.tipo == GRAMA).length;

        this.infoPokemons.pokemonsEletrico =
          pokemons.filter(
            (pokemon: Pokemon) => pokemon.tipo == ELETRICO).length;
      });

  }


}
