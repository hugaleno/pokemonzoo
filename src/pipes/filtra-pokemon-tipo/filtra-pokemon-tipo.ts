import { Pokemon } from './../../models/pokemon';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtraPokemonTipo',
})
export class FiltraPokemonTipoPipe implements PipeTransform {
  
  transform(pokemons: Pokemon[], tipo: string) {
    return pokemons.filter( 
      (pokemon: Pokemon, index:number, pokemons:Pokemon[]) => 
        pokemon.tipo == tipo
    )
  }
}
