import { Pokemon } from './../../models/pokemon';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable()
export class PokemonProvider {

  constructor(
    private storage: Storage
  ) { }

  getAll(): Promise<Pokemon[]> {
    return this.storage.ready()
      .then(() => {
        let pokemons: Pokemon[] = [];

        return this.storage.forEach(
          (pokemon: Pokemon, key: string, iterationNumber: number) => {
            //debugger; Aciona o debugger do browser
            if (key.indexOf("pokemons.") > -1) {
              pokemons.push(pokemon);
            }
          }).then(() => {
            return pokemons;
          });
      })
  }

  save(pokemon: Pokemon): Promise<Pokemon> {
    return this.storage.set(`pokemons.${pokemon.id}`, pokemon);
  }

  deletar(pokemon: Pokemon): Promise<Pokemon> {
    return this.storage.remove(`pokemons.${pokemon.id}`);
  }

  deletePorAmbiente(ambienteId: number): Promise<number> {
     return this.storage.ready()
      .then( () => {
        let numPokemonsExcluidos: number = 0;
         return this.storage.forEach( 
            (pokemon: Pokemon, key:string, iterationNumber: number) => {
            if(key.indexOf("pokemons.") > -1){
              if(pokemon.ambienteId == ambienteId){
                numPokemonsExcluidos++;
                this.deletar(pokemon);
              }
              
            }
          }).then( () => numPokemonsExcluidos);
      })
  }
}
