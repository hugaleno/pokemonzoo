import { PokemonProvider } from './../pokemon-provider/pokemon.provider';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Ambiente } from '../../models/ambiente';

@Injectable()
export class AmbienteProvider {

  constructor(
    private storage: Storage,
    private pokemonProvider:PokemonProvider
  ) {
  }

  getAll(): Promise<Ambiente[]> {
    return this.storage.ready()
      .then( () => {
        let ambientes: Ambiente[] = [];

        return this.storage.forEach( 
        (ambiente: Ambiente, key: string, iterationNumber:number) => {
            if( key.indexOf(`ambientes.`) > -1){
              ambientes.push(ambiente);
            }
        }).then(() => ambientes);
      });
  }

  save( ambiente: Ambiente ): Promise<Ambiente> {
    return this.storage.set(`ambientes.${ambiente.id}`, ambiente);
  }

  excluir( ambiente: Ambiente): Promise<Ambiente> {
    return this.storage.remove(`ambientes.${ambiente.id}`)
      .then( () => {
        
        this.pokemonProvider.deletePorAmbiente(ambiente.id)
        .then((numeroDePokemons: number)=> {
          if(numeroDePokemons == 1){
            console.log(numeroDePokemons+" pokemon foi apagado.");
          }else{
            console.log(numeroDePokemons+" pokemons foram apagados");
          }
          
        });
        return ambiente;
      });
  }


}
